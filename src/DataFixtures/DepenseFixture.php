<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use App\Entity\Depense;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Persistence\ObjectManager;


class DepenseFixture extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
            $user = new User();
            $user->setUsername("Utilisateur1");
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'mdp'
            ));

            $categorie = new Categories;
            $categorie->setNom("resto");
            $categorie->setLogo("superlogo");
            $categorie->setCouleur("red");
            $manager->persist($categorie);

            $manager->persist($user);
         $depense = new Depense();
         $depense->setNom("achat1");
         $depense->setPhoto("maPhoto");
         $depense->setSommes(52);
         $depense->setDate(\DateTime::createFromFormat('d-m-Y', "01-09-2015"));
         $depense->setUser($user);
         $depense->setCategorie($categorie);
         $manager->persist($depense);

         $depense2 = new Depense();
         $depense2->setNom("achat2");
         $depense2->setPhoto("maPhoto2");
         $depense2->setSommes(12);
         $depense2->setDate(\DateTime::createFromFormat('d-m-Y', "01-09-2015"));
         $depense2->setUser($user);
         $depense2->setCategorie($categorie);
         $manager->persist($depense2);

        $manager->flush();
    }
}
