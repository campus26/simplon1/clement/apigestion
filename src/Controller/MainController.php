<?php

namespace App\Controller;

use App\Repository\CategoriesRepository;
use App\Repository\DepenseRepository;
use App\Entity\Categories;
use App\Entity\Depense;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;


class MainController extends AbstractController
{
    /**
     * @Route("/depense", name="depense", methods={"GET"})
     */
    public function depense(DepenseRepository $depenseRepository): Response
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer]);

        $depense= $depenseRepository->findAll();

        $data = $serializer->normalize($depense, null, ['groups' => 'group1']);
   
        return $this->json($data);
    }

     /**
     * @Route("/addDepense", name="addDepense", methods={"POST"})
     */
    public function addDepense(Request $request,UserRepository $userRepository,CategoriesRepository $categoriesRepository): Response
    {
       $content= $request->getContent();
     $data = \json_decode($content, true);
        try {
            $object = $this->get('serializer')->deserialize($content, Depense::class, 'json');

            $user=$userRepository->findOneBy(array('id' => $data['user']));
            $categorie = $categoriesRepository->findOneBy(array('id' => $data['categorie']));
        
            $object->setUser($user);
            $object->setCategorie($categorie);
            
           } catch (\Throwable $th) {
            return new Response($th->getMessage(), 500);
           } 

           $entityManager = $this->getDoctrine()->getManager();
           $entityManager->persist($object);
           $entityManager->flush();
        return new Response("Ajout d'un score", 200);
    }





    /**
     * @Route("/categorie", name="categorie", methods={"GET"})
     */
    public function categorie(CategoriesRepository $CategoriesRepository): Response
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer]);
        $mesScore= $CategoriesRepository->findAll();
        $data = $serializer->normalize($mesScore, null, ['groups' => 'group1']);

     
        dump($data);
        return $this->json($data);

    }


     /**
     * @Route("/addCategorie", name="addCategorie", methods={"POST"})
     */
    public function addCategorie(Request $request): Response
    {
  
        $data = $request->getContent();
        try {
            $object = $this->get('serializer')->deserialize($data, Categories::class, 'json');
           } catch (\Throwable $th) {
            return new Response($th->getMessage(), 500);
           } 
           $entityManager = $this->getDoctrine()->getManager();
           $entityManager->persist($object);
           $entityManager->flush();
        return new Response("Ajout d'un score", 200);
    }




}

